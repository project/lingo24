Lingo 24
---------------------
We love languages at Lingo24. Every day we work hard to translate
documents, websites, tools and products, helping people all over the
world communicate with one another.

Translating your content can be challenging, but we aim to make it
very simple through easy to use tools, friendly people, and great
technology.

In terms of translation, **[Lingo24](http://www.lingo24.com)** offers a
range of professional human-translation and premium machine translation
services, all of which can accessed via our translation APIs.

This is a PHP interface to make using our APIs simpler.
Lingo 24 is a plugin for Translation Management Tool module (tmgmt) to
make using our services simpler.

REQUIREMENTS
------------
This module requires Drupal TMGMT (http://drupal.org/project/tmgmt) module
and a valid Lingo 24 API key.

INSTALLATION
------------
These instructions assume you have composer and drush installed.
For more information about installing composer and drush visit
(https://getcomposer.org/)
(https://www.drush.org/latest/)

1. Install TMGMT
  - `composer require drupal/tmgmt`
  - `drush en tmgmt`
2. Install Lingo 24
  - `composer require drupal/lingo24`
  - `drush en lingo24`

CONFIGURATION
------------
Configure Lingo24 Provider:
  - (Optional) Configure Lingo 24 permissions to allow other roles set
    the authentication keys.
  - Browse to `/admin/tmgmt/translators`
  - Drag priority to make **Lingo24** the top provider.
  - Click **Save**
  - Click **Edit** for the **Lingo24** provider
  - Add your **Lingo24 Client ID and Client Secret keys**.
  - Click **Save**

Configure your site to be translatable with TMGMT
  - Add a language to Drupal at /admin/config/regional/language/add
  - Enable the translations for the content type and the required fields
  at /admin/config/regional/content-language.
  - Create content of the content type where the translation was
  enabled.
  - Go to the content translation tab to translate your node. You can also
  perform bulk translations at /admin/tmgmt/sources.
  - Check the desired target language and click "Request translation"
  button. You will be redirected to the translation job page.
  - Set the specific settings for the job: Service Level, Translation Domain,
  Source and Target language regions.
  - Click "Submit to provider button.
  - Translations are automatically imported via cron. For importing the
  translation manually go to /admin/tmgmt/jobs and click Manage.
