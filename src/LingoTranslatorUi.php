<?php

namespace Drupal\lingo24;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StreamWrapper\StreamWrapperInterface;
use Drupal\Core\Url;
use Drupal\lingo24\Plugin\tmgmt\Translator\LingoTranslator;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt\TMGMTException;
use Drupal\tmgmt\TranslatorPluginUiBase;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Lingo24 Translator UI.
 */
class LingoTranslatorUi extends TranslatorPluginUiBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $formObject = $form_state->getFormObject();
    if (!($formObject instanceof EntityFormInterface)) {
      // Not an entity form, nothing to alter.
      return;
    }
    /** @var \Drupal\tmgmt\TranslatorInterface $translator */
    $translator = $formObject->getEntity();

    // Any visible, writeable wrapper can potentially be used for the files
    // directory, including a remote file system that integrates with a CDN.
    foreach (\Drupal::service('stream_wrapper_manager')->getDescriptions(StreamWrapperInterface::WRITE_VISIBLE) as $scheme => $description) {
      $options[$scheme] = Html::escape($description);
    }

    $form['lingo_environment'] = [
      '#type' => 'select',
      '#title' => $this->t('Environment'),
      '#default_value' => $translator->getSetting('lingo_environment') ?: 'demo',
      '#description' => $this->t('Select the default Lingo24 API Environment'),
      '#options' => [
        'demo' => $this->t('Demo'),
        'live' => $this->t('Live'),
      ],
      '#required' => FALSE,
    ];

    $form['lingo_client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#default_value' => $translator->getSetting('lingo_client_id') ?: '',
      '#description' => $this->t("Please enter your Lingo24 Client ID."),
      '#required' => TRUE,
    ];

    $form['lingo_client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client Secret'),
      '#default_value' => $translator->getSetting('lingo_client_secret') ?: '',
      '#description' => $this->t("Please enter your Lingo24 Client Secret."),
      '#required' => TRUE,
    ];

    if (!empty($options)) {
      $form['scheme'] = [
        '#type' => 'radios',
        '#title' => $this->t('Storage method'),
        '#default_value' => $translator->getSetting('scheme'),
        '#options' => $options,
        '#description' => $this->t('Choose the location where exported files should be stored. The usage of a protected location (e.g. private://) is recommended to prevent unauthorized access.'),
      ];
    }
    // Set API Redirect URI.
    $redirect_uri = Url::fromRoute('lingo24.oauth_callback', [], ['absolute' => TRUE])->toString();
    \Drupal::state()->set('lingo_redirect_uri', $redirect_uri);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function checkoutSettingsForm(array $form, FormStateInterface $form_state, JobInterface $job) {
    $translator = $job->getTranslator();
    $lingo24Docs = LingoTranslator::oauthClient($translator);

    $services = [];
    foreach ($lingo24Docs->getServices() as $service) {
      $services[$service->getId()] = $service->getName();
    }

    $form['lingo_project_id'] = [
      '#type' => 'hidden',
      '#title' => $this->t('Source Language Region'),
      '#value' => uniqid('Job-'),
    ];

    $form['lingo_services'] = [
      '#type' => 'select',
      '#title' => $this->t('Service'),
      '#description' => $this->t('Select a Lingo24 service.'),
      '#options' => $services,
      '#required' => TRUE,
    ];

    $domains = [];
    foreach ($lingo24Docs->getDomains() as $domain) {
      $domains[$domain->getId()] = $domain->getName();
    }

    $form['lingo_domains'] = [
      '#type' => 'select',
      '#title' => $this->t('Domain'),
      '#description' => $this->t('Select the domain area for translation.'),
      '#options' => $domains,
      '#required' => TRUE,
    ];

    // Fetch locales and match with remotes multiple regions languages.
    $source_laguage = $job->getSourceLangcode();
    $target_language = $job->getTargetLangcode();
    $locales = [];
    $remote_regions = self::getLocales();
    $remote_codes = [];

    foreach ($lingo24Docs->getLocales() as $locale) {
      $locales[$locale->getLanguage()]['regions'][] = $locale->getCountry();
      $remote_codes[] = $locale->getLanguage() . '-' . $locale->getCountry();
    }

    // Source Language Logic.
    if (strpos($source_laguage, '-') !== false) {
      foreach ($remote_codes as $remote_code) {
        if (strtolower($source_laguage) == strtolower($remote_code)) {
          $split_code = explode("-", $remote_code);
          $country = $split_code[0];
          $region = $split_code[1];
          $form['lingo_source_country'] = [
            '#type' => 'hidden',
            '#title' => $this->t('Source Language Country'),
            '#value' => $split_code[0],
          ];
          $form['lingo_source_region'] = [
            '#type' => 'hidden',
            '#title' => $this->t('Source Language Region'),
            '#value' => $split_code[1],
          ];
        }
      }
    }
    else {
      $form['lingo_source_country'] = [
        '#type' => 'hidden',
        '#title' => $this->t('Source Language Country'),
        '#value' => $source_laguage,
      ];
      if (array_key_exists($source_laguage, $locales)) {
        if (count($locales[$source_laguage]['regions']) > 1) {
          $source_regions = $remote_regions[$source_laguage];
          $form['lingo_source_region'] = [
            '#type' => 'select',
            '#title' => $this->t('Source Language Region'),
            '#description' => $this->t('Select the Region where the source language belongs to.'),
            '#options' => $source_regions,
            '#required' => TRUE,
          ];
        } else {
          $form['lingo_source_region'] = [
            '#type' => 'hidden',
            '#title' => $this->t('Source Language Region'),
            '#value' => $locales[$source_laguage]['regions'],
          ];
        }
      }
    }

    // Target Language Logic.
    if (strpos($target_language, '-') !== false) {
      foreach ($remote_codes as $remote_code){
        if (strtolower($target_language) == strtolower($remote_code)) {
          $split_code = explode("-", $remote_code);
          $country = $split_code[0];
          $region = $split_code[1];
          $form['lingo_target_country'] = [
            '#type' => 'hidden',
            '#title' => $this->t('Target Language Country'),
            '#value' => $split_code[0],
          ];
          $form['lingo_target_region'] = [
            '#type' => 'hidden',
            '#title' => $this->t('Target Language Region'),
            '#value' => $split_code[1],
          ];
        }
      }
    }
    else {
      $form['lingo_target_country'] = [
        '#type' => 'hidden',
        '#title' => $this->t('Target Language Country'),
        '#value' => $target_language,
      ];
      if (array_key_exists($target_language, $locales)) {
        if (count($locales[$target_language]['regions']) > 1) {
          $target_regions = $remote_regions[$target_language];
          $form['lingo_target_region'] = [
            '#type' => 'select',
            '#title' => $this->t('Target Language Region'),
            '#description' => $this->t('Select the Region where the target language belongs to.'),
            '#options' => $target_regions,
            '#required' => TRUE,
          ];
        } else {
          $form['lingo_target_region'] = [
            '#type' => 'hidden',
            '#title' => $this->t('Target Language Region'),
            '#value' => $locales[$target_language]['regions'],
          ];
        }
      }
    }

    return parent::checkoutSettingsForm($form, $form_state, $job);
  }

  /**
   * {@inheritdoc}
   */
  public function checkoutInfo(JobInterface $job) {
    // If the job is finished, it's not possible to import translations anymore.
    if ($job->isFinished()) {
      return parent::checkoutInfo($job);
    }

    $form = [
      '#type' => 'fieldset',
      '#title' => $this->t('Import translated file'),
    ];

    $translator = $job->getTranslator();

    // Set target translation environment.
    if ($translator->getSetting('lingo_environment') == 'demo') {
      $projects_uri = 'https://ease-demo.lingo24.com/projects';
    }
    else {
      $projects_uri = 'https://ease.lingo24.com/projects';
    }
    $project_page = $this->t('Check the project status <a href="@link">here</a>.', ['@link' => $projects_uri]);

    $lingo_pid = '';
    foreach ($job->getRemoteMappings() as $mapping) {
      $lingo_pid = $mapping->getRemoteIdentifier1();
    }

    // Fecth Lingo24 projects status.
    $lingo24Docs = LingoTranslator::oauthClient($translator);
    $project = $lingo24Docs->getProject($lingo_pid);
    switch ($project->getStatus()->getValue()) {
      case 'CREATED':
        $lingo_status = $this->t('The project has been created in Lingo24.') . $project_page;
        break;

      case 'QUOTED':
        $lingo_status = $this->t('The project has been quoted in Lingo24. A typical quote is valid for 15 days.') . $project_page;
        break;

      case 'IN_PROGRESS':
        $lingo_status = $this->t('Quote has been accepted in Lingo24. This job is in progress.') . $project_page;
        break;

      case 'CANCELLED':
        // If the job was cancelled remote, abort tmgmt job.
        try {
          if ($job->isAbortable()) {
            $job->setState(JobInterface::STATE_ABORTED, 'Translation job has been aborted.');
          }
        }
        catch (TMGMTException $e) {
          $job->addMessage('Failed to abort translation job. @error', ['@error' => $e->getMessage()], 'error');
        }

        return new RedirectResponse(Url::fromRoute('entity.tmgmt_job.canonical')->toString());

      case 'FINISHED':
        /* If the job is finished, pull the translated file and auto
          * accept the translation and its review.
          */
        $job->acceptTranslation();
        $lingo_status = $this->t('All of the project jobs have been completed');
        if ($job->isFinished() == FALSE) {
          foreach ($lingo24Docs->getJobs($project->getId()) as $lingo_job) {
            $name = "JobID" . $job->id() . '_' . $job->getSourceLangcode() . '_' . $job->getTargetLangcode();
            $file = $lingo_job->getTargetFile($lingo24Docs);
            $path = $job->getSetting('scheme') . '://lingo24/' . $name . '.xlf';
            $dirname = dirname($path);
            if (\Drupal::service('file_system')->prepareDirectory($dirname, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
              $file = file_save_data($lingo24Docs->getFileContent($file->getId()), $path, FileSystemInterface::EXISTS_REPLACE);
              $plugin = \Drupal::service('plugin.manager.lingo24.format')->createInstance('xlf');
              try {
                $job->addTranslatedData($plugin->import($path));
                $job->setValidationRequired(FALSE);
                $job->acceptTranslation();
                \Drupal::logger('lingo24')->info($this->t('Successfully imported file for translation job @id.', ['@id' => $job->id()]));
                \Drupal::logger('lingo24')->info($this->t('Successfully accepted translation job @id.', ['@id' => $job->id()]));
                return new RedirectResponse(Url::fromRoute('system.admin_content')->toString());
              }
              catch (\Exception $e) {
                \Drupal::logger('lingo24')->error($this->t('Failed importing file for translation job @id:@error', [
                  '@id' => $job->id(),
                  '@error' => $e->getMessage(),
                ]));
              }
            }
          }
        }
        return new RedirectResponse(Url::fromRoute('entity.tmgmt_job.canonical')->toString());
    }

    $supported_formats = array_keys(\Drupal::service('plugin.manager.lingo24.format')->getDefinitions());
    if (isset($lingo_status)) {
      $form['lingo_status'] = [
        '#prefix' => '<p>',
        '#suffix' => '</p>',
        '#markup' => $lingo_status,
        '#weight' => -100,
      ];
    }
    $form['file'] = [
      '#type' => 'file',
      '#title' => $this->t('File'),
      '#size' => 50,
      '#description' => $this->t('Supported formats: @formats.', ['@formats' => implode(', ', $supported_formats)]),
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import'),
      '#submit' => ['lingo24_import_form_submit'],
    ];
    return $this->checkoutInfoWrapper($job, $form);
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    if ($form_state->hasAnyErrors()) {
      return;
    }
    $formObject = $form_state->getFormObject();
    if (!($formObject instanceof EntityFormInterface)) {
      // Not an entity form, nothing to alter.
      return;
    }
    /** @var \Drupal\tmgmt\TranslatorInterface $translator */
    $translator = $formObject->getEntity();

    if (empty($translator->getSetting('lingo_client_id'))) {
      $form_state->setError($form['plugin_wrapper']['settings']['lingo_client_id'], $this->t('Please, introduce a valid Client ID.'));
    }
    if (empty($translator->getSetting('lingo_client_secret'))) {
      $form_state->setError($form['plugin_wrapper']['settings']['lingo_client_secret'], $this->t('Please, introduce a valid Client Secret.'));
    }
    // Check if the authorization code exist and request one if doesn't.
    if (empty(\Drupal::state()->get('lingo_auth_Code'))) {
      if ($translator->getSetting('lingo_environment') == 'demo') {
        $uri = \Lingo24\API\Docs::$AUTH_URL_DEMO . '?response_type=code';
      }
      else {
        $uri = \Lingo24\API\Docs::$AUTH_URL_LIVE . '?response_type=code';
      }
      $uri .= '&client_id=' . $translator->getSetting('lingo_client_id');
      $uri .= '&redirect_uri=' . \Drupal::state()->get('lingo_redirect_uri');
      $redirect = new RedirectResponse($uri);
      $redirect->send();
    }
  }

  /**
   * Fecth remote regions for multi region locales.
   *
   * @return array
   *   $regions
   */
  public static function getLocales() {
    $regions_path = \Drupal::service('module_handler')->getModule('lingo24')->getPath() . '/regions/remoteRegions.json';
    $regions = json_decode(file_get_contents($regions_path), TRUE);
    return $regions;
  }

}
