<?php

namespace Drupal\lingo24\Format;

use Drupal\tmgmt\JobInterface;

/**
 * Interface for exporting to a given file format.
 */
interface FormatInterface {

  /**
   * Return the file content for the job data.
   *
   * @param Drupal\tmgmt\JobInterface $job
   *   The translation job object to be exported.
   * @param array $conditions
   *   (optional) An array containing list of conditions.
   *
   * @return string
   *   String with the file content.
   */
  public function export(JobInterface $job, array $conditions = []);

  /**
   * Validates that the given file is valid and can be imported.
   *
   * @param mixed $imported_file
   *   File path to the file to be imported.
   * @param bool $is_file
   *   (optional) Whether $imported_file is the path to a file or not.
   *
   * @return Drupal\tmgmt\Entity\Job
   *   Returns the corresponding translation job entity if the import file is
   *   valid, FALSE otherwise.
   */
  public function validateImport($imported_file, $is_file = TRUE);

  /**
   * Converts an exported file content back to the translated data.
   *
   * @param string $imported_file
   *   Path to a file or an XML string to import.
   * @param bool $is_file
   *   (optional) Whether $imported_file is the path to a file or not.
   *
   * @return array
   *   Translated data array.
   */
  public function import($imported_file, $is_file = TRUE);

}
