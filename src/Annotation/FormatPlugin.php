<?php

namespace Drupal\lingo24\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Format plugin annotation object.
 *
 * @Annotation
 *
 * @see \Drupal\lingo24\Format\FormatManager
 */
class FormatPlugin extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the format.
   *
   * @var \Drupal\Core\Annotation\Translation
   * @ingroup plugin_translatable
   */
  public $label;

}
