<?php

namespace Drupal\lingo24\Plugin\tmgmt\Translator;

use Drupal\Core\File\FileSystemInterface;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt\Translator\TranslatableResult;
use Drupal\tmgmt\TranslatorInterface;
use Drupal\tmgmt\TranslatorPluginBase;
use Lingo24\API\Docs;
use Lingo24\API\Model\OAuth2;
use Lingo24\API\ProjectWizard;

/**
 * Lingo24 Translator.
 *
 * @TranslatorPlugin(
 *   id = "lingo24",
 *   label = @Translation("Lingo24 Translator"),
 *   description = @Translation("Lingo24 Translation Plugin."),
 *   logo = "icons/lingo24.png",
 *   ui = "Drupal\lingo24\LingoTranslatorUi"
 * )
 */
class LingoTranslator extends TranslatorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function checkTranslatable(TranslatorInterface $translator, JobInterface $job) {
    // Anything can be exported.
    return TranslatableResult::yes();
  }

  /**
   * {@inheritdoc}
   */
  public function requestTranslation(JobInterface $job) {
    // Set file name.
    $name = "JobID" . $job->id() . '_' . $job->getSourceLangcode() . '_' . $job->getTargetLangcode();
    // Create format instance.
    $export = \Drupal::service('plugin.manager.lingo24.format')->createInstance('xlf', []);
    // Set file path and dierctory name.
    $path = $job->getSetting('scheme') . '://lingo24/' . $name . '.xlf';
    $dirname = dirname($path);
    // Process translation.
    if (\Drupal::service('file_system')->prepareDirectory($dirname, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
      // Create the client.
      $translator = $job->getTranslator();
      $lingo24Docs = self::oauthClient($translator);
      // Save the file.
      $file = file_save_data($export->export($job), $path, FileSystemInterface::EXISTS_REPLACE);
      // Prepare and submit translation file to Lingo24.
      if (!empty($job->getSetting('lingo_source_country')) && !empty($job->getSetting('lingo_source_region')) && !empty($job->getSetting('lingo_target_country')) && !empty($job->getSetting('lingo_target_region'))) {
        $source_locale = $lingo24Docs->getLocale($job->getSetting('lingo_source_country') . '_' . $job->getSetting('lingo_source_region'));
        $target_locale = $lingo24Docs->getLocale($job->getSetting('lingo_target_country') . '_' . $job->getSetting('lingo_target_region'));
        $lingo24Project = (new ProjectWizard('Drupal Job: ' . $job->id()))
          ->setDomain($lingo24Docs->getDomain($job->getSetting('lingo_domains')))
          ->setService($lingo24Docs->getService($job->getSetting('lingo_services')))
          ->setSourceLocale($source_locale)
          ->addTag('Drupal')
          ->addFile($path, $name . '.xlf')
          ->addTargetLocale($target_locale)
          ->create($lingo24Docs);
        $lingo24Docs->quoteProject($lingo24Project);
        $project_id = $lingo24Project->getId();
        foreach ($job->getItems() as $item) {
          $item->addRemoteMapping('remote_id', $project_id);
        }
        \Drupal::service('file.usage')->add($file, 'lingo24', 'tmgmt_job', $job->id());
        $job->submitted('File submitted to Lingo24. Please review the project quote <a href="@link">here</a>.', ['@link' => 'https://ease-demo.lingo24.com/projects']);
      }
      else {
        if (empty($job->getSetting('lingo_source_country')) || empty($job->getSetting('lingo_source_region'))) {
          $job->rejected('The source language is not translatable by Lingo24. Please contact Lingo24 support.');
        }
        else {
          $job->rejected('The target language is not translatable by Lingo24. Please contact Lingo24 support.');
        }
      }

    }
    else {
      $job->rejected('Failed to create writable directory @dirname, check file system permissions.', ['@dirname' => $dirname]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultSettings() {
    return [
      'export_format' => 'xlf',
      'allow_override' => TRUE,
      'scheme' => 'public',
      'format_configuration' => [],
    ];
  }

  /**
   * Create an OAuth2 Client from a Translator.
   *
   * @param mixed $translator
   *   Container of Translator settings.
   *
   * @return \Lingo24\API\Docs
   *   Lingo24 Client.
   */
  public static function oauthClient($translator) {
    // Init state API.
    $state = \Drupal::state();
    // Create OAuth Object.
    $oauth2 = self::oauthCredentials($state);
    // Configure OAuth.
    $client_id = $translator->getSetting('lingo_client_id');
    $client_secret = $translator->getSetting('lingo_client_secret');
    $env = $translator->getSetting('lingo_environment');
    $redirect_uri = $state->get('lingo_redirect_uri');
    // Init OAuth and store credentials.
    $lingo24Docs = new Docs($client_id, $client_secret, $redirect_uri, $oauth2, $env);
    $state->set('lingo_access_token', $lingo24Docs->getOAuth2()->getAccessToken());
    $state->set('lingo_refresh_token', $lingo24Docs->getOAuth2()->getRefreshToken());
    $state->set('lingo_token_expire', $lingo24Docs->getOAuth2()->getExpiryDate());
    return $lingo24Docs;
  }

  /**
   * Create an OAuth2 model object from a state API data.
   *
   * @param mixed $state
   *   Container with credentials settings.
   *
   * @return Object
   *   $oauth2 API model.
   */
  public static function oauthCredentials($state) {
    $oauth2 = new \stdClass();
    $oauth2->access_token = $state->get('lingo_access_token');
    $oauth2->refresh_token = $state->get('lingo_refresh_token');
    $oauth2->expires_in = $state->get('lingo_token_expire') - time();
    return new OAuth2($oauth2);
  }

}
