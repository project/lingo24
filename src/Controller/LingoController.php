<?php

namespace Drupal\lingo24\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\State\State;
use Lingo24\API\Docs;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Handles callback routes for Lingo24 module.
 */
class LingoController extends ControllerBase {
  /**
   * The messenger interface.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The state API.
   *
   * @var Drupal\Core\State\State
   */
  protected $state;

  /**
   * The config interface.
   *
   * @var Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * The request.
   *
   * @var Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The logger factory.
   *
   * @var Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $logger;

  /**
   * Constructor.
   *
   * @param Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger interface.
   * @param Drupal\Core\State\State $state
   *   The state interface.
   * @param Drupal\Core\Config\ConfigFactoryInterface $config
   *   The config interface.
   * @param Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param Drupal\Core\Logger\LoggerChannelFactory $logger
   *   The logger factory.
   */
  public function __construct(MessengerInterface $messenger, State $state, ConfigFactoryInterface $config, Request $request, LoggerChannelFactory $logger) {
    $this->messenger = $messenger;
    $this->state = $state;
    $this->config = $config;
    $this->request = $request;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('state'),
      $container->get('config.factory'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('logger.factory')
    );
  }

  /**
   * Authorize Lingo24 API.
   *
   * @return Redirect
   *   Redirect to Lingo24 Provider Settings.
   */
  public function oauthCallback() {
    // Get Authorization code from URL.
    $auth_code = $this->request->query->get('code') ?: '';
    if ($auth_code != '') {
      // Init state API.
      $state = $this->state;
      // Store Athorization code.
      $state->set('lingo_auth_Code', $auth_code);
      $this->messenger()->addMessage($this->t('Your site has been authorized on Lingo24.'));
    }
    else {
      $this->messenger()->addError($this->t('Lingo24 authorization failed, please review your creditials.'));
    }
    // Redirect to provider configuration page.
    return $this->redirect('entity.tmgmt_translator.edit_form', ['tmgmt_translator' => 'lingo24']);
  }
  /**
   * Authenticate Lingo24 API.
   *
   * @return Redirect
   *   Redirect to Lingo24 Provider Settings.
   */
  public function oauthTokenCallback() {
    $state = $this->state;
    $auth_code = $state->get('lingo_auth_Code');
    // Init configFactory.
    $config_factory = $this->config;
    $config = $config_factory->getEditable('tmgmt.translator.lingo24');
    // Configure OAuth.
    $client_id = $config->get('settings.lingo_client_id');
    $client_secret = $config->get('settings.lingo_client_secret');
    $env = $config->get('settings.lingo_environment');
    $redirect_uri = $state->get('lingo_redirect_uri');
    // Init OAuth and store credentials.
    $client = new Docs($client_id, $client_secret, $redirect_uri, $auth_code, $env);
    $state->set('lingo_access_token', $client->getOAuth2()->getAccessToken());
    $state->set('lingo_refresh_token', $client->getOAuth2()->getRefreshToken());
    $state->set('lingo_token_expire', $client->getOAuth2()->getExpiryDate());
    // Add success message.
    $this->messenger()->addMessage($this->t('Your site has been authenticated on Lingo24.'));
    // Redirect to provider configuration page.
    return $this->redirect('entity.tmgmt_translator.edit_form', ['tmgmt_translator' => 'lingo24']);
  }

}
