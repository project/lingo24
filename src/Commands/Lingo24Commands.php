<?php

namespace Drupal\lingo24\Commands;

use Drupal\Core\File\FileSystemInterface;
use Drush\Commands\DrushCommands;
use Drupal\lingo24\Format\FormatManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Drush commandfile.
 */
class Lingo24Commands extends DrushCommands {

  /**
   * The File System.
   *
   * @var Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The Format Plugin.
   *
   * @var Drupal\lingo24\Format\FormatManager
   */
  protected $format;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The File System.
   * @param Drupal\lingo24\Format\FormatManager $format
   *   The Format Plugin.
   */
  public function __construct(FileSystemInterface $file_system, FormatManager $format) {
    $this->file_system = $file_system;
    $this->format = $format;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('file_system'),
      $container->get('plugin.manager.lingo24.format')
    );
  }

  /**
   * Import XLIFF translation files manually via command line.
   *
   * @param mixed $name
   *   Directory path that is search for *.xlf files or a file name.
   *
   * @command tmgmt_translate_import
   * @aliases tmti
   *
   * @throws \Exception
   *   If there is no file or if the file isn't accessible, throws an exception.
   */
  public function tmgmtTranslateImport($name) {
    if (!file_exists($name)) {
      // Drush changes the current working directory to drupal root directory.
      // Also check the current directory.
      if (!file_exists(getcwd() . '/' . $name)) {
        throw new \Exception(dt('@name does not exists or is not accessible.', ['@name' => $name]));
      }
      else {
        // The path is relative to the current directory, update the variable.
        $name = getcwd() . '/' . $name;
      }
    }

    if (is_dir($name)) {
      $this->logger()->notice(dt('Scanning dir @dir.', ['@dir' => $name]));
      $files = $this->file_system->scanDirectory($name, '/.*\.xlf$/');
      if (empty($files)) {
        throw new \Exception(dt('No files found to import in @name.', ['@name' => $name]));
      }
    }
    else {
      // Create the structure expected by the loop below.
      $files = [$name => (object) ['name' => basename($name)]];
    }

    $plugin = $this->format->createInstance('xlf');
    foreach ($files as $path => $info) {
      $job = $plugin->validateImport($path);
      if (empty($job)) {
        $this->logger()->error(dt('No translation job found for @filename.', ['@filename' => $info->name]));
        continue;
      }

      if ($job->isFinished()) {
        $this->logger()->warning(dt('Skipping @filename for finished job @name (#@id).', [
          '@filename' => $info->name,
          '@name' => $job->label(),
          '@id' => $job->id(),
        ]));
        continue;
      }

      try {
        // Validation successful, start import.
        $job->addTranslatedData($plugin->import($path));
        $this->logger()->notice(dt('Successfully imported file @filename for translation job @name (#@id).', [
          '@filename' => $info->name,
          '@name' => $job->label(),
          '@id' => $job->id(),
        ]));
      }
      catch (\Exception $e) {
        $this->logger()->error(dt('Failed importing file @filename: @error', [
          '@filename' => $info->name,
          '@error' => $e->getMessage(),
        ]));
      }
    }
  }

}
