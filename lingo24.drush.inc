<?php

/**
 * @file
 * Drush integration for lingo24.
 */

use Drush\Drush;

/**
 * Implements hook_drush_command().
 */
function lingo24_drush_command() {
  $items = [];

  $items['tmgmt_translate_import'] = [
    'description' => 'Import XLIFF translation files',
    'arguments' => [
      'name'    => 'Directory path that is search for *.xlf files or a file name',
    ],
    'aliases' => ['tmti'],
  ];

  return $items;
}

/**
 * Import XLIFF files from a directory or single file.
 */
function drush_lingo24_tmgmt_translate_import($name = NULL) {
  if (!$name) {
    return \Drupal::logger(dt('You need to provide a directory path or filename.'));
  }

  if (!file_exists($name)) {
    // Drush changes the current working directory to the drupal root directory.
    // Also check the current directory.
    if (!file_exists(Drush::config()->cwd() . '/' . $name)) {
      return \Drupal::logger(dt('@name does not exists or is not accessible.', ['@name' => $name]));
    }
    else {
      // The path is relative to the current directory, update the variable.
      $name = Drush::config()->cwd() . '/' . $name;
    }
  }

  if (is_dir($name)) {
    \Drupal::logger(dt('Scanning dir @dir.', ['@dir' => $name]), 'success');
    $files = \Drupal::service('file_system')->scanDirectory($name, '/.*\.xlf$/');
    if (empty($files)) {
      \Drupal::logger(dt('No files found to import in @name.', ['@name' => $name]));
    }
  }
  else {
    // Create the structure expected by the loop below.
    $files = [$name => (object) ['name' => basename($name)]];
  }

  $plugin = \Drupal::service('plugin.manager.lingo24.format')->createInstance('xlf');
  foreach ($files as $path => $info) {
    $job = $plugin->validateImport($path);
    if (empty($job)) {
      \Drupal::logger(dt('No translation job found for @filename.', ['@filename' => $info->name]), 'error');
      continue;
    }

    if ($job->isFinished()) {
      \Drupal::logger(dt('Skipping @filename for finished job @name (#@id).', [
        '@filename' => $info->name,
        '@name' => $job->label(),
        '@id' => $job->id(),
      ]), 'warning');
      continue;
    }

    try {
      // Validation successful, start import.
      $job->addTranslatedData($plugin->import($path));
      \Drupal::logger(dt('Successfully imported file @filename for translation job @name (#@id).', [
        '@filename' => $info->name,
        '@name' => $job->label(),
        '@id' => $job->id(),
      ]), 'success');
    }
    catch (Exception $e) {
      \Drupal::logger(dt('Failed importing file @filename: @error', [
        '@filename' => $info->name,
        '@error' => $e->getMessage(),
      ]), 'error');
    }
  }
}
