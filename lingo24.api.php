<?php

/**
 * @file
 * API and hook documentation for the Lingo24 Translator module.
 */

/**
 * Alter file format plugins provided by other modules.
 */
function hook_lingo24_format_plugin_info_alter(&$file_formats) {
  // Switch the used HTML plugin controller class.
  $file_formats['html']['class'] = '\Drupal\mymodule\DifferentHtmlImplementation';
}

/**
 * Provide information about available text processors.
 *
 * @return array
 *   An array of available text processor definitions. The key is the text
 *   processor name.
 */
function hook_lingo24_text_processor_plugin_info() {
  return [
    'mask_html_for_xliff' => [
      'label' => t('Escape HTML'),
      'processor class' => 'Lingo24XLIFFMaskHTMLProcessor',
    ],
  ];
}
